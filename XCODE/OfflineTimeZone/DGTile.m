//
//  DGTile.m
//  OfflineTimeZone
//
//  Created by Daniel Iglesias on 13/08/15.
//  Copyright (c) 2015 Daniel Gaston. All rights reserved.
//

#import "DGTile.h"

@implementation DGTile

- (id) initWithTop:(double)top
            bottom:(double)bottom
              left:(double)left
             right:(double)right
{
    
    self = [super init];
    if (self)
    {
        self.top = top;
        self.bottom = bottom;
        self.left = left;
        self.right = right;

    }
    return self;
}


@end
