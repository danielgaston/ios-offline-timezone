//
//  DGTimeZone.h
//  OfflineTimeZone
//
//  Created by Daniel Iglesias on 12/08/15.
//  Copyright (c) 2015 Daniel Gaston. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface DGTimeZone : NSObject

- (NSString *)ZoneIdFromDBForLocation:(CLLocation *)location;
+ (DGTimeZone *)sharedInstance;

@end
