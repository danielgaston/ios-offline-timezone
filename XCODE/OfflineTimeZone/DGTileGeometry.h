//
//  DGTileGeometry.h
//  OfflineTimeZone
//
//  Created by Daniel Iglesias on 13/08/15.
//  Copyright (c) 2015 Daniel Gaston. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DGTileGeometry : NSObject

@property(readwrite,assign) NSInteger tileId;
@property(readwrite,copy) NSString *name;
@property(readwrite,copy) NSString *geometry;

- (id) initWithTileId:(NSInteger)tileId
         timeZoneName:(NSString *)tzName
     timeZoneGeometry:(NSString *)tzGeom;

@end
