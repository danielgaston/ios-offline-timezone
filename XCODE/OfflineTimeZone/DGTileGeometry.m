//
//  DGTileGeometry.m
//  OfflineTimeZone
//
//  Created by Daniel Iglesias on 13/08/15.
//  Copyright (c) 2015 Daniel Gaston. All rights reserved.
//

#import "DGTileGeometry.h"

@implementation DGTileGeometry

- (id) initWithTileId:(NSInteger)tileId
         timeZoneName:(NSString *)tzName
     timeZoneGeometry:(NSString *)tzGeom
{
    
    self = [super init];
    if (self)
    {
        self.tileId = tileId;
        self.name = tzName;
        self.geometry = tzGeom;
    }
    return self;
}


@end
