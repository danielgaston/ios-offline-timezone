//
//  DGTimeZone.m
//  OfflineTimeZone
//
//  Created by Daniel Iglesias on 12/08/15.
//  Copyright (c) 2015 Daniel Gaston. All rights reserved.
//

#import "DGTimeZone.h"
#import "DGDatabaseManager.h"

@interface DGTimeZone ()

@property (nonatomic, strong) NSArray *timeZonesDB;

@end


@implementation DGTimeZone

#pragma mark - Public

- (NSString *)timeZoneIdFromDBWithLocation:(CLLocation *)location
{
    return [self ZoneIdFromDBForLocation:location];
}

#pragma mark - Private

- (NSString *)ZoneIdFromDBForLocation:(CLLocation *)location
{
    NSString *tzId;
    DGDatabaseManager *dbm = [DGDatabaseManager sharedInstance];
    NSInteger gridId = [dbm gridIdForLocation:location];
    NSMutableDictionary *tzGeoms =[dbm tileGeometriesforTileId:gridId];
    BOOL isInside = NO;
    
    for (NSString *shapeId in tzGeoms)
    {
        
        DGTileGeometry *tile = [tzGeoms objectForKey:shapeId];
        
        NSString *shapeGeomStr = [tile geometry];

        NSArray *coordinates = [NSJSONSerialization JSONObjectWithData:[shapeGeomStr dataUsingEncoding:NSUTF8StringEncoding] options:NSUTF8StringEncoding error:nil];
        
        double lon = location.coordinate.longitude;
        double lat = location.coordinate.latitude;
        
        CGPoint mapPointAsCGP = CGPointMake(lon, lat);
        CGMutablePathRef mpr = CGPathCreateMutable();
        
        // TODO: Only fetched first geom without holes
        if (coordinates) {
            coordinates = [coordinates objectAtIndex:0];
        }
        
        for (int i=0; i < coordinates.count; i++){
            
            NSArray *point = [coordinates objectAtIndex:i];
            
            NSNumber *lonNum = [point objectAtIndex:0];
            NSNumber *latNum = [point objectAtIndex:1];
            
            CGFloat lonCoord = [lonNum floatValue];
            CGFloat latCoord = [latNum floatValue];
            
            if (i == 0)
                CGPathMoveToPoint(mpr, NULL, lonCoord, latCoord);
            else
                CGPathAddLineToPoint(mpr, NULL, lonCoord, latCoord);
        }
        
        if(CGPathContainsPoint(mpr , NULL, mapPointAsCGP, FALSE))
            isInside = YES;
        
        CGPathRelease(mpr);
        
        if (isInside) {
            
            tzId = [tile name];
            break;
        }
    }

    return tzId;
}


#pragma mark - Singleton
+ (DGTimeZone *)sharedInstance
{
    static dispatch_once_t pred;
    static DGTimeZone *sharedInstance = nil;
    dispatch_once(&pred, ^{
        sharedInstance = [[[self class] alloc] init];
    });
    return sharedInstance;
}

@end
