//
//  DGDatabaseManager.h
//  OfflineTimeZone
//
//  Created by Daniel Iglesias on 13/08/15.
//  Copyright (c) 2015 Daniel Gaston. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import <CoreLocation/CoreLocation.h>
#import "DGTile.h"
#import "DGTileGeometry.h"

@interface DGDatabaseManager : NSObject
{
    sqlite3 *database;
}

@property (nonatomic,strong) NSString *dbFullPath;

+ (DGDatabaseManager *)sharedInstance;

- (void)openDb;
- (void)closeDb;

- (NSInteger)gridIdForLocation:(CLLocation*)location;
- (NSMutableDictionary *)tileGeometriesforTileId:(NSInteger)tileId;

@end
