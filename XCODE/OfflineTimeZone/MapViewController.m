//
//  MapKit.m
//  OfflineTimeZone
//
//  Created by Daniel Iglesias on 12/08/15.
//  Copyright (c) 2015 Daniel Gaston. All rights reserved.
//

#import "MapViewController.h"
#import "DGTimeZone.h"
#import <QuartzCore/QuartzCore.h>

@interface MapViewController ()

@property DGTimeZone *timeZone;
@property NSNumberFormatter *formatter;

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.map.delegate = self;
    self.timeZone = [DGTimeZone new];
    
    self.formatter = [[NSNumberFormatter alloc] init];
    [self.formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [self.formatter setMaximumFractionDigits:6];
    [self.formatter setRoundingMode: NSNumberFormatterRoundDown];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - MapView Delegates
- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    
    CLLocation *location = [[CLLocation alloc] initWithLatitude:mapView.centerCoordinate.latitude
                                                      longitude:mapView.centerCoordinate.longitude];
    
    self.latLabel.text = [self.formatter stringFromNumber:[NSNumber numberWithFloat:location.coordinate.latitude]];
    self.lonLabel.text = [self.formatter stringFromNumber:[NSNumber numberWithFloat:location.coordinate.longitude]];
    
    NSString *tzId = [self.timeZone ZoneIdFromDBForLocation:location];
    self.tzStringLabel.text = tzId;
    
    if (tzId) {
        NSTimeZone *timeZoneObj = [NSTimeZone timeZoneWithName:tzId];
        self.tzLabel.text = [NSString stringWithFormat:@"%@\nDST: %@\nGMT Seconds: %ld\nDST Seconds: %ld",
                             [timeZoneObj name],
                             [timeZoneObj isDaylightSavingTime] ? @"true" : @"false",
                             (long)[timeZoneObj secondsFromGMT],
                             (long)[[NSNumber numberWithDouble:[timeZoneObj daylightSavingTimeOffset]] integerValue]];
        
        NSLog(@"TZ:\t%@",tzId);
    } else {
        NSString *noTZ = @"No Timezone for Location";
        self.tzStringLabel.text = noTZ;
        self.tzLabel.text = noTZ;
        NSLog(@"TZ:\t%@",noTZ);
        
    }
    
    NSLog(@"****************************************************************");
    [self showMarker:location];
}

#pragma mark - private

- (void) showMarker:(CLLocation *)location
{
    // Remove previous annotations
    [self.map removeAnnotations:self.map.annotations];
    // Drop an annotation
    MKPointAnnotation *startPin = [[MKPointAnnotation alloc] init];
    startPin.coordinate = location.coordinate;
    startPin.title = @"TZ";
    [self.map addAnnotation:startPin];
}

@end
