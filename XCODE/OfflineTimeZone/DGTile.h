//
//  DGTile.h
//  OfflineTimeZone
//
//  Created by Daniel Iglesias on 13/08/15.
//  Copyright (c) 2015 Daniel Gaston. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DGTile : NSObject

@property(readwrite,assign) NSInteger identif;
@property(readwrite,assign) double top;
@property(readwrite,assign) double bottom;
@property(readwrite,assign) double left;
@property(readwrite,assign) double right;

- (id) initWithTop:(double)top
            bottom:(double)bottom
              left:(double)left
             right:(double)right;

@end
