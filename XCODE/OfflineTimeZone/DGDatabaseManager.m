//
//  DGDatabaseManager.m
//  OfflineTimeZone
//
//  Created by Daniel Iglesias on 13/08/15.
//  Copyright (c) 2015 Daniel Gaston. All rights reserved.
//

#import "DGDatabaseManager.h"

@implementation DGDatabaseManager

static DGDatabaseManager *shared;

+ (DGDatabaseManager *)sharedInstance
{
    static dispatch_once_t pred;
    
    dispatch_once(&pred, ^{
        shared = [[DGDatabaseManager alloc] init];

        NSString *lib = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *dbs = [lib stringByAppendingPathComponent:@"Database"];
        NSString *file = [dbs stringByAppendingPathComponent:@"timezone.sqlite"];
        if(![[NSFileManager defaultManager] fileExistsAtPath:file]) {
            
            id dbInBundle = [[NSBundle mainBundle] pathForResource:@"timezone" ofType:@"sqlite"];
            
            [[NSFileManager defaultManager] createDirectoryAtPath:dbs withIntermediateDirectories:YES attributes:nil error:nil];
            [[NSFileManager defaultManager] copyItemAtPath:dbInBundle toPath:file error:nil];
        }
        shared.dbFullPath = file;
        if (sqlite3_open([shared.dbFullPath UTF8String], &shared->database) != SQLITE_OK) shared = NULL;

    });
    
    return shared;
}

- (void)openDb
{
    if (database == NULL)
    {
        sqlite3 *newDBConnection;
        
        if (sqlite3_open([self.dbFullPath UTF8String], &newDBConnection) == SQLITE_OK) database = newDBConnection;
        else database = NULL;
    }
}

- (void)closeDb
{
    sqlite3_close(database);
}

- (NSInteger)gridIdForLocation:(CLLocation *)location
{
    /***********************
     Coordinates pattern: (lon,lat)
     Grid tile pattern:
     
           Bottom
             +
             |
     Left +-----+ Right
             |
             +
            Top
     ***********************/
    
    CLLocationDegrees lat = location.coordinate.latitude;
    CLLocationDegrees lon = location.coordinate.longitude;
    
    CLLocationDegrees bottomBounding = (ceil(location.coordinate.latitude/10)*10);
    CLLocationDegrees topBounding = (floor(location.coordinate.latitude/10)*10);
    
    topBounding = (topBounding == bottomBounding && topBounding >= 0) ? bottomBounding - 10 : topBounding;
    bottomBounding = (topBounding == bottomBounding && topBounding < 0) ? topBounding + 10 : bottomBounding;
    
    CLLocationDegrees rightBounding = (ceil(location.coordinate.longitude/10)*10);
    CLLocationDegrees leftBounding = (floor(location.coordinate.longitude/10)*10);
    
    leftBounding = (rightBounding == leftBounding && rightBounding >= 0) ? rightBounding - 10 : leftBounding;
    rightBounding = (rightBounding == leftBounding && rightBounding < 0) ? rightBounding + 10 : rightBounding;
    
    NSLog(@"****************************************************************");
    NSLog(@"lat:\t%f top:\t%f bottom:\t%f",lat, topBounding, bottomBounding);
    NSLog(@"lon:\t%f left:\t%f right:\t%f",lon, leftBounding, rightBounding);
    
    sqlite3_stmt *statement;
    const char *dbpath = [self.dbFullPath UTF8String];
    NSString *querySQL = [NSString stringWithFormat: @"SELECT * FROM tile WHERE (bottom=%f AND top=%f AND left=%f AND right=%f);", bottomBounding, topBounding, leftBounding, rightBounding];
    const char *query_stmt = [querySQL UTF8String];
    
    NSNumber *gridIdNum;
    // db open
    if (sqlite3_open(dbpath, &database) == SQLITE_OK){
        // db prepare query
        if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            // db query result
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                // db extract query result column value
                gridIdNum = [NSNumber numberWithInteger:sqlite3_column_int(statement, 0)];
            }
            sqlite3_finalize(statement);
        } else {
            NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(database));
        }
        // db close
        sqlite3_close(database);
    }

    return [gridIdNum integerValue];
}

- (NSMutableDictionary *)tileGeometriesforTileId:(NSInteger)tileId
{
    NSMutableDictionary *result = [[NSMutableDictionary alloc] init];
    
    sqlite3_stmt *statement;
    const char *dbpath = [self.dbFullPath UTF8String];
    NSString *querySQL = [NSString stringWithFormat: @"SELECT * FROM tile_geometry WHERE tile_id=%ld;", (long)tileId];
    const char *query_stmt = [querySQL UTF8String];
    
    // db open
    if (sqlite3_open(dbpath, &database) == SQLITE_OK){
        // db prepare query
        if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            // db query result
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                // db extract query result column value
                NSNumber *shapeId = [NSNumber numberWithInt:sqlite3_column_int(statement, 0)];
                NSNumber *tileId = [NSNumber numberWithInt:sqlite3_column_int(statement, 1)];
                NSString *tzName = [NSString  stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)];
                NSString *tzGeom = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
                
                DGTileGeometry *tileGeom = [[DGTileGeometry alloc] initWithTileId:[tileId integerValue] timeZoneName:tzName timeZoneGeometry:tzGeom];
                [result setObject:tileGeom forKey:[shapeId description]];
            }
            sqlite3_finalize(statement);
        } else {
            NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(database));
        }
        // db close
        sqlite3_close(database);
    }
    
    return result;
}

@end
