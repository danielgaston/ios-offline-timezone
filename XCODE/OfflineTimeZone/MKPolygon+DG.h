//
//  MKPolygon+DG.h
//  OfflineTimeZone
//
//  Created by Daniel Iglesias on 12/08/15.
//  Copyright (c) 2015 Daniel Gaston. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MKPolygon (DG)
-(BOOL) pointInPolygon:(CLLocationCoordinate2D) point mapView: (MKMapView*) mapView;
@end
