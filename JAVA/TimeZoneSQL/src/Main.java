import java.util.HashMap;

public class Main {
	
	public static void main(String[] args) {
		
		HashMap<String,String> config = new Utils().getConfig();
		SqlBuilder tilebuilder = new TileSqlBuilder();
		SqlBuilder tileGeometryBuilder = new TileGeometrySqlBuilder();
		tilebuilder.build(config);
		tileGeometryBuilder.build(config);
	}
	
	
	
}
