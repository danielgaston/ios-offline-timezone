import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class TileGeometrySqlBuilder implements SqlBuilder {

	HashMap<String,String> configuration;
	String insertSql = new String();
	int counter;

	@Override
	public void build(HashMap<String,String> config) {
		// TODO Auto-generated method stub

		configuration = config;
		
		JSONParser parser = new JSONParser();
		ArrayList<TileGeometry> content = new ArrayList<>();
		try {
			System.out.println("*********************************");
			System.out.println("Starting to parse JSON...");
			Object obj = parser.parse(new FileReader(configuration.get(Utils.FILE_PATH_IN) + configuration.get(Utils.TILE_GEOM_JSON_FILE_NAME) + ".geojson"));

			System.out.println("Parsing finished.");
			JSONObject jsonObject = (JSONObject) obj;
			JSONArray features = (JSONArray) jsonObject.get("features");
			Iterator<JSONObject> iterator = features.iterator();

			counter = features.size();

			while (iterator.hasNext()) {
				JSONObject featureInfo = iterator.next();

				JSONObject featureProp = (JSONObject) featureInfo.get("properties");
				JSONObject featureGeom = (JSONObject) featureInfo.get("geometry");

				String featureTimezone = (String) featureProp.get("TZID");
				int featureTileId = ((Double)featureProp.get("tz_id")).intValue();

				JSONArray featureCoordParent = (JSONArray) featureGeom.get("coordinates");
				String featureTileGeom = featureCoordParent.toJSONString();

				TileGeometry tileGeom = new TileGeometry();
				tileGeom.setTileId(featureTileId);
				tileGeom.setTimeZoneId(featureTimezone);
				tileGeom.setGeometry(featureTileGeom);

				content.add(tileGeom);
				counter--;
				System.out.println("Shape Processed. " + counter + " remaining.");
			}
			System.out.println("*********************************");
			createSQLInserts(content);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void createSQLInserts(ArrayList<TileGeometry> content) {
		System.out.println("*********************************");
		System.out.println("creating SQL string...");
		Iterator<TileGeometry> tileIterator = content.iterator();
		PrintWriter writer;
		try {
			writer = new PrintWriter(configuration.get(Utils.FILE_PATH_OUT) + configuration.get(Utils.TILE_GEOM_SQL_FILE_NAME) + ".sql", "UTF-8");
			
			writer.println("DROP TABLE IF EXISTS tile_geometry;");
			writer.println("CREATE TABLE tile_geometry (\"id\" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , \"tile_id\" INTEGER NOT NULL ,\"timezone\" STRING, \"geom\" BLOB);");
			
			
			counter = content.size();
			while (tileIterator.hasNext()) {

				TileGeometry tileGeomObj = tileIterator.next();

				String tileTimezoneId = tileGeomObj.getTimeZoneId();
				String tileTileId = String.valueOf(tileGeomObj.getTileId());
				String tileGeomCoords = tileGeomObj.getGeometries();

				String str = String.format("INSERT INTO tile_geometry(tile_id,timezone,geom) VALUES (%s,\"%s\",\"%s\");",
						tileTileId,tileTimezoneId,tileGeomCoords);

				counter--;
				System.out.println("SQL string created. " + counter + " remaining.");
				writer.println(str);
				System.out.println("SQL string written. " + counter + " remaining.");
			}

			writer.close();
			System.out.println("File writting ended");
			System.out.println("*********************************");
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	

	private class TileGeometry {

		String timeZoneId;
		int tileId;
		String geometries;

		public String getTimeZoneId() {
			return timeZoneId;
		}
		public void setTimeZoneId(String timeZoneId) {
			this.timeZoneId = timeZoneId;
		}
		public int getTileId() {
			return tileId;
		}
		public void setTileId(int tileId) {
			this.tileId = tileId;
		}
		public String getGeometries() {
			return geometries;
		}
		public void setGeometry(String geometry) {
			this.geometries = geometry;
		}
	}

}
