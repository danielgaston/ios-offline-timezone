
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class TileSqlBuilder implements SqlBuilder {

	HashMap<String,String> configuration;
	String insertSql = new String();
	int counter;

	@Override
	public void build(HashMap<String,String> config) {

		configuration = config;
		
		JSONParser parser = new JSONParser();
		ArrayList<Tile> content = new ArrayList<>();
		try {
			System.out.println("*********************************");
			System.out.println("Starting to parse JSON...");
			Object obj = parser.parse(new FileReader(configuration.get(Utils.FILE_PATH_IN) + configuration.get(Utils.TILE_JSON_FILE_NAME) + ".geojson"));

			System.out.println("Parsing finished.");
			JSONObject jsonObject = (JSONObject) obj;
			JSONArray features = (JSONArray) jsonObject.get("features");
			Iterator<JSONObject> iterator = features.iterator();

			counter = features.size();

			while (iterator.hasNext()) {
				JSONObject featureInfo = iterator.next();

				JSONObject featureProp = (JSONObject) featureInfo.get("properties");
								
				int featureTzId = ((Long) featureProp.get("tz_id")).intValue();
				double featureLeft = ((Double) featureProp.get("left")).doubleValue();
				double featureTop = ((Double) featureProp.get("top")).doubleValue();
				double featureRight = ((Double) featureProp.get("right")).doubleValue();
				double featureBottom = ((Double) featureProp.get("bottom")).doubleValue();

				Tile tile = new Tile();
				tile.setTileId(featureTzId);
				tile.setLeft(featureLeft);
				tile.setTop(featureTop);
				tile.setRight(featureRight);
				tile.setBottom(featureBottom);
				
				content.add(tile);
				counter--;
				System.out.println("Shape Processed. " + counter + " remaining.");
			}
			System.out.println("*********************************");
			createSQLInserts(content);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void createSQLInserts(ArrayList<Tile> content) {
		System.out.println("*********************************");
		System.out.println("creating SQL string...");
		Iterator<Tile> tileIterator = content.iterator();
		PrintWriter writer;
		try {
			writer = new PrintWriter(configuration.get(Utils.FILE_PATH_OUT) + configuration.get(Utils.TILE_SQL_FILE_NAME) + ".sql", "UTF-8");
			writer.println("DROP TABLE IF EXISTS tile;");
			writer.println("CREATE TABLE tile (\"id\" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , \"top\" DOUBLE NOT NULL , \"bottom\" DOUBLE NOT NULL , \"left\" DOUBLE NOT NULL , \"right\" DOUBLE NOT NULL );");
			counter = content.size();
			while (tileIterator.hasNext()) {

				Tile tileGeomObj = tileIterator.next();
				
				int featureTzId = tileGeomObj.getTileId();
				double featureLeft = tileGeomObj.getLeft();
				double featureTop = tileGeomObj.getTop();
				double featureRight = tileGeomObj.getRight();
				double featureBottom = tileGeomObj.getBottom();
				
				String str = String.format("INSERT INTO tile(id,top,bottom,left,right) VALUES (%s,%s,%s,%s,%s);",
						featureTzId,featureTop,featureBottom,featureLeft,featureRight);

				counter--;
				System.out.println("SQL string created. " + counter + " remaining.");
				writer.println(str);
				System.out.println("SQL string written. " + counter + " remaining.");
			}

			writer.close();
			System.out.println("File writting ended");
			System.out.println("*********************************");
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	

	private class Tile {

		int tileId;
		double left;
		double top;
		double right;
		double bottom;
		
		public int getTileId() {
			return tileId;
		}
		public void setTileId(int tileId) {
			this.tileId = tileId;
		}
		public double getLeft() {
			return left;
		}
		public void setLeft(double left) {
			this.left = left;
		}
		public double getTop() {
			return top;
		}
		public void setTop(double top) {
			this.top = top;
		}
		public double getRight() {
			return right;
		}
		public void setRight(double right) {
			this.right = right;
		}
		public double getBottom() {
			return bottom;
		}
		public void setBottom(double bottom) {
			this.bottom = bottom;
		}

	}

}
