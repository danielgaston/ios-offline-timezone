import java.util.HashMap;

public interface SqlBuilder {
	void build(HashMap<String,String> config);
}
