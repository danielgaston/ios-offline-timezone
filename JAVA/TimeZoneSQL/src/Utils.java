import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

public class Utils {

	public static final String FILE_PATH_IN = "file_path_IN";
	public static final String FILE_PATH_OUT = "file_path_OUT";
	public static final String TILE_JSON_FILE_NAME = "tile_json_file_name";
	public static final String TILE_SQL_FILE_NAME = "tile_sql_file_name";
	public static final String TILE_GEOM_JSON_FILE_NAME = "tile_geometry_json_file_name";
	public static final String TILE_GEOM_SQL_FILE_NAME = "tile_geometry_sql_file_name";
	
public HashMap<String,String> getConfig() {
		
		HashMap<String,String> config = new HashMap<String,String>();
		InputStream inputStream = null;
		try {
			Properties prop = new Properties();
			String propFileName = "config.properties";
 
			inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
 
			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}
 
			// get the property value and print it out
			
			String filePathIN = prop.getProperty(FILE_PATH_IN);
			String filePathOUT = prop.getProperty(FILE_PATH_OUT);
			String tileJsonFile = prop.getProperty(TILE_JSON_FILE_NAME);
			String tileSqlFileName = prop.getProperty(TILE_SQL_FILE_NAME);
			String tileGeometryJsonFileName = prop.getProperty(TILE_GEOM_JSON_FILE_NAME);
			String tileGeometrySqlFileName = prop.getProperty(TILE_GEOM_SQL_FILE_NAME);
			
			if (filePathIN == null) { throw new RuntimeException("filePathIN value not found in config file");} else { config.put(FILE_PATH_IN, filePathIN);}
			if (filePathOUT == null) { throw new RuntimeException("filePathOUT value not found in config file");} else { config.put(FILE_PATH_OUT, filePathOUT);}
			if (tileJsonFile == null) { throw new RuntimeException("tileJsonFile value not found in config file");} else { config.put(TILE_JSON_FILE_NAME, tileJsonFile);}
			if (tileSqlFileName == null) { throw new RuntimeException("tileSqlFileName value not found in config file");} else { config.put(TILE_SQL_FILE_NAME, tileSqlFileName);}
			if (tileGeometryJsonFileName == null) { throw new RuntimeException("tileGeometryJsonFileName value not found in config file");} else { config.put(TILE_GEOM_JSON_FILE_NAME, tileGeometryJsonFileName);}
			if (tileGeometrySqlFileName == null) { throw new RuntimeException("tileGeometrySqlFileName value not found in config file");} else { config.put(TILE_GEOM_SQL_FILE_NAME, tileGeometrySqlFileName);}
 
			System.out.println("CONFIGURATION");
			System.out.println("---------------------------------------------------");
			System.out.println("filePathIN == " + filePathIN);
			System.out.println("filePathOUT == " + filePathOUT);
			System.out.println("tileJsonFile == " + tileJsonFile);
			System.out.println("tileSqlFileName == " + tileSqlFileName);
			System.out.println("tileGeometryJsonFileName == " + tileGeometryJsonFileName);
			System.out.println("tileGeometrySqlFileName == " + tileGeometrySqlFileName);
			System.out.println("---------------------------------------------------\n");
			
			inputStream.close();
			
		} catch (Exception e) {
			System.out.println("Exception: " + e);
		} 
		
		return config;
	}
	
}
